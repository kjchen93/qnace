#include "triangle.h"

Triangle::Triangle()
{
    //random start rotation
    angle = (rand() % 360);
    //set speed
    speed = 2;
    setRotation(angle);

    //start position
    int startX = 200;
    int startY= 50;

    if((rand() % 1)){
        startX = rand() % 10 + 200;
        startY = rand() % 30 + 40 ;
    } else{
        startX = rand() % -100 -20;
        startY = rand() % -100 - 43;
    }

    setPos(mapToParent(startX,startY));
}

QRectF Triangle::boundingRect() const
{
     return QRect(0,0,20,20);

}

void Triangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rec = boundingRect();
    QBrush brush(Qt::white);
    if(!scene()->collidingItems(this).isEmpty()){
        DoCollision();
    }
    // Draw Triangle
    QPainterPath path;
    path.moveTo(rec.left() + (rec.width() / 2), rec.top());
    path.lineTo(rec.bottomLeft());
    path.lineTo(rec.bottomRight());
    path.lineTo(rec.left() + (rec.width() / 2), rec.top());

    painter->setPen(Qt::NoPen);
    painter->fillPath(path, brush);
    painter->drawRect(rec);
}

void Triangle::advance(int phase)
{
    if(!phase) return;
    //QPointF location = this->pos();

    setPos(mapToParent(0,-(speed)));
}

void Triangle::DoCollision()
{
    if(rand()%1){
        setRotation(rotation() + (180 + (rand()%10)));
    } else {
        setRotation(rotation() + (180 - (rand()%10)));
    }

    QPointF newpoint = mapToParent(-(boundingRect().width()), -(boundingRect().width()+2));

    if(!scene()->sceneRect().contains(newpoint)){
        newpoint = mapToParent(0,0);
    } else {
        setPos(newpoint);
    }

}
