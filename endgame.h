#ifndef ENDGAME_H
#define ENDGAME_H

#include <QWidget>

namespace Ui {
class Endgame;
}

class Endgame : public QWidget
{
    Q_OBJECT

public:
    explicit Endgame(QWidget *parent = nullptr);
    ~Endgame();

private:
    Ui::Endgame *ui;
};

#endif // ENDGAME_H
