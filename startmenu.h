#ifndef STARTMENU_H
#define STARTMENU_H

#include <QMainWindow>
#include "questionparser.h"
#include <QPropertyAnimation>
#include <QtCore>
#include <QtGui>
#include <QGraphicsScene>
namespace Ui {
class StartMenu;
}

class StartMenu : public QMainWindow
{
    Q_OBJECT

public:
    explicit StartMenu(QWidget *parent = nullptr);
    ~StartMenu();

private:
    Ui::StartMenu *ui;
    QuestionParser question_parser = QuestionParser(":/quizes");
    QPropertyAnimation* animation;
    QGraphicsScene* scenes;
    QTimer *timer;
private slots:
    void StartQuiz();
};

#endif // STARTMENU_H
