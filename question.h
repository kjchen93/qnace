#ifndef QUESTION_H
#define QUESTION_H

#include <QVector>
#include <QString>

class Question
{

private:

    QString text;
    QVector<QString> answers;
    int solution;

public:
    Question(QString Q, QVector<QString> A, int sol);
    QString getQuestion();
    int getSolution();
    QVector<QString> getAnswers();
};

#endif // QUESTION_H
