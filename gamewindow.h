#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include <QVector>
#include <QTimer>
#include <QTime>
#include "question.h"
namespace Ui {
class gamewindow;
}

class gamewindow : public QWidget
{
    Q_OBJECT

public:
    explicit gamewindow(QWidget *parent = nullptr,QVector<Question> questions={});
    ~gamewindow();


    void displayScore();
    void displayStreak();
    void addScore(double);
    void addStreak(bool);


private slots:
    void answer(int answer);
    void answerButton1Clicked();
    void answerButton2Clicked();
    void answerButton3Clicked();
    void answerButton4Clicked();
    void displayQuestion();
    void displayTime();
private:
    Ui::gamewindow *ui;
    QVector<Question> questions;
    int index;
    QTimer* timer;
    QTimer* feedback_timer;
    QTime time_remainding;
    int streak;
    int hstreak;
    double score;
};

#endif // GAMEWINDOW_H
