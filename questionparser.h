#ifndef QUESTIONPARSER_H
#define QUESTIONPARSER_H

#include "question.h"
#include<QVector>
#include<QString>

class QuestionParser
{
public:
    QuestionParser(QString resourcePath);
    QVector<QString> getQuizes();
    bool quizToFile(QVector<Question>& questions, QString filename);
    bool quizFromFile(QVector<Question>& questions, QString filename);
private:
    QString resourcesPath;
};

#endif // QUESTIONPARSER_H
