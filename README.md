# QnAce

## Description
A Qt quiz application designed for accelerated learning. Users can create their own study set or use existing data sets to learn any topic of interest.

## UML Diagram
An early outline of the project is shown in the UML diagram below.

![](./imgs/qnace_uml.jpg)


## Gameplay
The game allows users to select a various number of topics to study. Once a topic is selected the background of the topic will be highlighted. The game starts when the user clicks on the `Start Learning` button after selecting the topic to study. During the game the questions will be prompted, and the user can choose one of four alternatives as answer for the questions. Correct answer will immdietly turn the button green and red for incorrect answers. The users will be given the results at the end of the game. 
![Start Menu](./imgs/startmenu.jpg)
![](./imgs/rightans.jpg)
![](./imgs/wrongans.jpg)
![](./imgs/results.jpg)


## Authors
This project is proudly presented by team KOK, Experis Academy.

Team KOK: 
- [Karl Munthe](https://gitlab.com/karl.munthe)
- [Ole Kjepso](https://gitlab.com/olemikole)
- [Kai Chen](https://gitlab.com/kjchen93/)