#include "startmenu.h"
#include <QApplication>
#include "questionparser.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    StartMenu sm;
    sm.show();
    return a.exec();
}
