#include "user.h"


user::user()
{

}

void user::setScore(double score){
    scoreVec.push_back(score);
}
void user::setName(QString userName){
    QString name = userName;
}
void user::setCorrectAnswer(int i){
    correctAnswer.push_back(i);
}
void user::setTimeUsed(double t){
    timeUsed.push_back(t);
}

int user::getNumCorr(){     //returns number of correct answers
    int numCorr = 0;
    for(int i = 0; i < scoreVec.size(); i++){
        if(scoreVec[i] == 1){
            numCorr++;
        }
    }
    return numCorr;
}

double user::getPercCorr(){     //returns percent of questions user answered correctly
    double numCorr = user::getNumCorr();
    double percCorr = numCorr/scoreVec.size();
    return percCorr;
}

double user::getMeanTime(){     //returns average time used on each question
    double sumTime = 0;
    for(int i = 0; i < timeUsed.size(); i++){
        sumTime += timeUsed[i];
    }
    double meanTime = sumTime/timeUsed.size();
    return meanTime;
}

double user::getFastestTime(){
    double fastTime = 0;
    for(int i = 0; i < timeUsed.size(); i++){
        if(timeUsed[i] > fastTime){
            fastTime = timeUsed[i];
        }
        else{}
    }
    return fastTime;
}

QVector<int> user::getCorrectAnswer(){
    return correctAnswer;
}
QVector<double> user::getTimeUsed(){
    return timeUsed;
}
QVector<double> user::getScoreVec(){
    return scoreVec;
}
QString user::getName(){
    return name;
}
