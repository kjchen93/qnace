#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "digitalclock.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    DigitalClock clock(ui->widget_timer);

}

MainWindow::~MainWindow()
{
    delete ui;
}

