#include "question.h"

Question::Question(QString t, QVector<QString> as, int sol){
    text = t;
    answers = as;
    solution = sol;
};

QString Question::getQuestion(){
    return text;
};

int Question::getSolution(){
    return solution;
}

QVector<QString> Question::getAnswers()
{
    return answers;
}
