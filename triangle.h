#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>

class Triangle : public QGraphicsItem
{
public:
    Triangle();
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget *widget);

protected:
    void advance(int phase);

private:
    qreal angle;
    qreal speed;
    void DoCollision();
};

#endif // TRIANGLE_H
