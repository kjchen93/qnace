#include "gamewindow.h"
#include "gamewindow.h"
#include "ui_gamewindow.h"
#include "user.h"
#include <QThread>
#include <QMessageBox>
#include "startmenu.h"



gamewindow::gamewindow(QWidget *parent,QVector<Question> questions) :
    QWidget(parent),
    ui(new Ui::gamewindow),
    questions(questions)
{
    ui->setupUi(this);
    index = 0;
    if(index == 0){
        score = 0;
        streak = 0;
        hstreak= 0;
    }

    displayQuestion();

    connect(ui->answerBtn_1,&QPushButton::pressed,this,&gamewindow::answerButton1Clicked, Qt::DirectConnection);
    connect(ui->answerBtn_2,&QPushButton::clicked,this,&gamewindow::answerButton2Clicked);
    connect(ui->answerBtn_3,&QPushButton::clicked,this,&gamewindow::answerButton3Clicked);
    connect(ui->answerBtn_4,&QPushButton::clicked,this,&gamewindow::answerButton4Clicked, Qt::QueuedConnection);

    timer = new QTimer(this);
    connect(timer,&QTimer::timeout,this,&gamewindow::displayTime);
    connect(timer,&QTimer::timeout,this,&gamewindow::displayStreak);
    connect(timer,&QTimer::timeout,this,&gamewindow::displayScore);
    timer->start(1000);
    time_remainding = QTime(0,0,20);
    displayTime();
    displayScore();
    displayStreak();
}

void gamewindow::displayTime()
{
    time_remainding = time_remainding.addSecs(-1);
    if(time_remainding.second()==0)
        answer(-1);
    ui->label_time->setText(time_remainding.toString("ss"));
}

void gamewindow::answer(int answer) // -1 means no answer
{
    if(answer == questions[index].getSolution())
    {
        //CalculateScore(true,time_remainding,)
        switch(answer){
        case 0:
            ui->answerBtn_1->setStyleSheet("background-color: green;");
            ui->answerBtn_1->repaint();
            QThread::msleep(1000);
            ui->answerBtn_1->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 1:
            ui->answerBtn_2->setStyleSheet("background-color: green;");
            ui->answerBtn_2->repaint();
            QThread::msleep(1000);
            ui->answerBtn_2->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 2:
            ui->answerBtn_3->setStyleSheet("background-color: green;");
            ui->answerBtn_3->repaint();
            QThread::msleep(1000);
            ui->answerBtn_3->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 3:
            ui->answerBtn_4->setStyleSheet("background-color: green;");
            ui->answerBtn_4->repaint();
            QThread::msleep(1000);
            ui->answerBtn_4->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        }
        double timeUsed = 20.0 - time_remainding.second();
        addScore(timeUsed);
        bool corr = true;
        addStreak(corr);
        if (streak > hstreak){
            hstreak = streak;
        }
    }
    else if(answer==-1)
    {
        QTextStream(stdout)<<"Time ran out\n";
        bool corr = false;
        addStreak(corr);
    }
    else
    {
        switch(answer){
        case 0:
            ui->answerBtn_1->setStyleSheet("background-color: red;");
            ui->answerBtn_1->repaint();
            QThread::msleep(1000);
            ui->answerBtn_1->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 1:
            ui->answerBtn_2->setStyleSheet("background-color: red;");
            ui->answerBtn_2->repaint();
            QThread::msleep(1000);
            ui->answerBtn_2->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 2:
            ui->answerBtn_3->setStyleSheet("background-color: red;");
            ui->answerBtn_3->repaint();
            QThread::msleep(1000);
            ui->answerBtn_3->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        case 3:
            ui->answerBtn_4->setStyleSheet("background-color: red;");
            ui->answerBtn_4->repaint();
            QThread::msleep(1000);
            ui->answerBtn_4->setStyleSheet("background-color: rgb(244, 173, 4);");
            break;
        }
        bool corr = false;
        addStreak(corr);
    }

    index++;
    if(index<questions.length())
        displayQuestion();
    else{
        //QApplication::quit();
        QString result = "Score: " + QString::number(score) + "\n" + "Highest Streak:" + QString::number(hstreak);
        this->close();
        QMessageBox::information(this, "Results", result);
        QApplication::quit();

    }
    time_remainding=QTime(0,0,20);
}

void gamewindow::answerButton1Clicked()
{
    answer(0);
}

void gamewindow::answerButton2Clicked()
{
    answer(1);
}

void gamewindow::answerButton3Clicked()
{
    answer(2);
}

void gamewindow::answerButton4Clicked()
{
    answer(3);
}


void gamewindow::displayQuestion()
{

    auto q = questions[index];
    ui->label_question_text->setText(q.getQuestion());
    auto ans = q.getAnswers();
    ui->answerBtn_1->setText(ans[0]);
    ui->answerBtn_2->setText(ans[1]);
    ui->answerBtn_3->setText(ans[2]);
    ui->answerBtn_4->setText(ans[3]);
}

void gamewindow::displayScore(){
    ui->label_score_2->setText(QString::number(score));
    ui->label_score_2->update();
}

void gamewindow::displayStreak(){
    ui->label_streak_2->setText(QString::number(streak));
    ui->label_streak_2->update();
}

void gamewindow::addScore(double timeUsed){
    double scoreIncrease = 1000*(1 - std::pow(timeUsed/20, 2));
    qDebug() << scoreIncrease;
    score += scoreIncrease;
}
void gamewindow::addStreak(bool corr){
    if(corr == true){
        streak += 1;
    }
    else{
        streak = 0;
    }
    qDebug() << streak;
}

gamewindow::~gamewindow()
{
    delete ui;
}
