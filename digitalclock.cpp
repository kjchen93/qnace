#include "digitalclock.h"

#include <QTime>
#include <QTimer>


DigitalClock::DigitalClock(QWidget *parent)
    : QLCDNumber(parent)
{
    setSegmentStyle(Filled);

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this,&DigitalClock::showTime) ;
    timer->start(1000);
    showTime();

    setWindowTitle(tr("Time Remaining"));
}

void DigitalClock::showTime()
{
    static QTime time(0,0,21);
    time = time.addSecs(-1);
    QString text = time.toString("ss");
    display(text);
    QTextStream(stdout)<<time.toString("m:ss");
}
