#include "questionparser.h"

#include "question.h"

#include <QDirIterator>
#include <QJsonDocument>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>

QuestionParser::QuestionParser(QString path)
    : resourcesPath(path)
{}

QVector<QString> QuestionParser::getQuizes()
{
    QDirIterator it(resourcesPath,QDirIterator::Subdirectories);
    QVector<QString> result;
    while(it.hasNext())
    {
        QString tmp = it.next();
        result.push_back(tmp.mid(resourcesPath.size()+1,tmp.size()-resourcesPath.size()-6 )) ;
    }
    return result;
}

bool QuestionParser::quizToFile(QVector<Question> &questions, QString name)
{
    QFile saveFile(resourcesPath+"/"+ name+".json");
    if (!saveFile.open(QIODevice::WriteOnly)) {
            qWarning("Couldn't open save file.");
            return false;
    }
    QJsonObject gameObject;
    QJsonArray questionsJson;
    for(auto& q: questions)
    {
        QJsonObject questionJson;
        questionJson["text"] = q.getQuestion();
        questionJson["solution"] = q.getSolution();
        QJsonArray answersJson;
        for(auto& answer: q.getAnswers())
        {
            answersJson.push_back(answer);
        }
        questionJson["answers"] = answersJson;
        questionsJson.push_back(questionJson);
    }
    gameObject["questions"] = questionsJson;
    saveFile.write(QJsonDocument(gameObject).toJson());
    return true;
}

bool QuestionParser::quizFromFile(QVector<Question> &questions, QString name)
{
    QFile loadFile(resourcesPath+"/"+ name+".json");
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }
    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonArray jsonQuestions = loadDoc["questions"].toArray();
    for(int i=0;i<jsonQuestions.size();i++)
    {
        QJsonObject qJson = jsonQuestions[i].toObject();

        auto text = qJson["text"].toString();
        auto solution = qJson["solution"].toInt();
        QJsonArray answersJson = qJson["answers"].toArray();

        QVector<QString> answers;
        for(int i=0;i<answersJson.size();i++)
        {
            answers.push_back(answersJson[i].toString());
        }

        questions.emplace_back(text,answers,solution);
    }
    return true;
}
