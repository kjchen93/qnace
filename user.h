#ifndef USER_H
#define USER_H

#include <QString>
#include <vector>
#include <QVector>

class user
{


private:
    QString name;
    QVector<double> scoreVec;
    double time_end;
    QVector<int> correctAnswer; //vector of times user answered correctly 1, or falsly 0.
    QVector<double> timeUsed; //vector containing time user used to answer question.

public:
    user();
    void setScore(double time);
    void setName(QString);
    void setCorrectAnswer(int i);
    void setTimeUsed(double t);

    QVector<double> getScoreVec();
    QString getName();
    int getNumCorr();
    QVector<int> getCorrectAnswer();
    QVector<double> getTimeUsed();
    double getPercCorr();
    double getMeanTime();
    double getFastestTime();
};

#endif // USER_H

