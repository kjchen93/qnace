#include "startmenu.h"
#include "ui_startmenu.h"
#include "gamewindow.h"
#include "triangle.h"
StartMenu::StartMenu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StartMenu)
{
    ui->setupUi(this);
    this->setFixedSize(750,550);
    this->setWindowTitle("QnAce" );
    scenes = new QGraphicsScene(this);
    ui->graphicsView->setScene(scenes);

    ui->graphicsView->setRenderHint(QPainter::Antialiasing);

    scenes->setSceneRect(-20,-83,700,80);

    QPen mypen = QPen(qRgb(244, 173, 4));

    QLineF TopLine(scenes->sceneRect().topLeft(),scenes->sceneRect().topRight());
    QLineF LeftLine(scenes->sceneRect().topLeft(),scenes->sceneRect().bottomLeft());
    QLineF RightLine(scenes->sceneRect().topRight(),scenes->sceneRect().bottomRight());
    QLineF BottomLine(scenes->sceneRect().bottomLeft(),scenes->sceneRect().bottomRight());

    scenes->addLine(TopLine,mypen);
    scenes->addLine(LeftLine,mypen);
    scenes->addLine(RightLine,mypen);
    scenes->addLine(BottomLine,mypen);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), scenes, SLOT(advance()));
    timer->start(30);

    int iCount = 20;

    for(int i = 0; i < iCount; i++){
        Triangle *tri = new Triangle();
        scenes->addItem(tri);
    }
    connect(ui->start_pushButton ,&QPushButton::clicked,this,&StartMenu::StartQuiz);
}

void StartMenu::StartQuiz()
{

    QVector<Question> questions;
    QString selected_quiz;
    if (ui->pushButton_capital->isChecked()){
        selected_quiz = "Capitals";
    }else if(ui->pushButton_president->isChecked()){
        selected_quiz = "Presidents";
    }
    bool ok = question_parser.quizFromFile(questions,selected_quiz);
    if(!ok)
        return;
    this->setCentralWidget(new gamewindow(this,questions));
}
StartMenu::~StartMenu()
{
    delete ui;
}
